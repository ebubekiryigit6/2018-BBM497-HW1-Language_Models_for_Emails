# Language Models for E-mails

BBM497 - Introduction to NLP Lab.

Assignment 1

### Author
Ebubekir Yiğit - 21328629



### Project Introduction
First, the emails.csv file is read and all email bodies are retrieved. 
Sixty percent of e-mails are trained according to the N-gram model. 
Then the probability and perplexity values of the test E-mails are calculated. 
With the created N-gram models, 30 smoothed, 30 unsmoothed E-mails are generated. 
Likelihood and perplexity values are also calculated in the generated e-mails. 
The generated e-mails and calculated values are written to the given file.
## Getting Started

There are two approaches in the project. 
As shown in the file LanguageModel.py, 
it can create the desired model with the supplied train set, N-Gram and isSmoothed parameters. 
You can then use the generate mail function to generate the desired number of E-mails to given file.

**All operations are progressively saved to the console.log file. 
You can browse the console.log file to see the operations.**
```
    trigram_model = model.LanguageModel(sentences=trainSet,
                                        N_gram=model.TRIGRAM,
                                        smoothed=True,
                                        file=outputFile).createModel()
```

At the same time, the probablity and perplexity values of the test set are calculated in the LanguageModel.py file. 
After creating this model, the function "calculatePerplexityAndProbabilityFor" is used.

```
    # sum and len values for speed of calculation
    
    trigram_model.calculatePerplexityAndProbabilityFor(
        emails=testSetEmails,
        unigramDict=unigram_model.getModelFrequency(),
        bigramDict=bigram_model.getModelFrequency(),
        uni_len=unigram_model.getModelFrequencyLen(),
        bi_len=bigram_model.getModelFrequencyLen())
```

The other approach LanguageModel_v2.py statically holds the Unigram, 
Bigram and Trigram models and processes them accordingly. 
This model is slower in Unigram operation than other models and faster in other processes than other model.

This model is created with createModel function. 
The Generate E-mails command generates Unigram, Bigram and Trigram E-mails to given file.

```
    model_v2.createModel(trainSet)
    model_v2.generateEmails(smoothed=True, howMany=10, filePath=path)
    model_v2.generateEmails(smoothed=False, howMany=10, filePath=path)
```

### Prerequisites

Python 3.5 is required to run the project. 
I have not used any Python external libraries.


For Ubuntu:

```
sudo apt-get install python3.5
```

For Centos:
```
sudo yum -y install python35u
```

### Running the Project

python3 assignment1.py <mails_csv_file> <output_file>

Example:
```
python3 assignment1.py emails.csv results.txt
```

**After run, please see "console.log" file to show process logs.**

