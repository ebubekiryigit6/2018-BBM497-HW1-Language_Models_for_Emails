import sys

import EmailParser as mailparser
import LanguageModel as model
import LanguageModel_v2 as model_v2
import utils


def calculations(argv, trainSet, testSetEmails):
    outputFile = open(argv[2], "w")
    generateCount = 10

    # part 1 training
    unigram_model = model.LanguageModel(sentences=trainSet,
                                        N_gram=model.UNIGRAM,
                                        smoothed=True,
                                        file=outputFile).createModel()

    bigram_model = model.LanguageModel(sentences=trainSet,
                                       N_gram=model.BIGRAM,
                                       smoothed=True,
                                       file=outputFile).createModel()

    trigram_model = model.LanguageModel(sentences=trainSet,
                                        N_gram=model.TRIGRAM,
                                        smoothed=True,
                                        file=outputFile).createModel()

    # part 2 Add-One (Laplace) Smoothing
    # but calculates perplexity and probability of Trigram and Bigram
    # It is against the structure of the object-oriented but solves the problem of speed.
    trigram_model.calculatePerplexityAndProbabilityFor(
        emails=testSetEmails,
        unigramDict=unigram_model.getModelFrequency(),
        bigramDict=bigram_model.getModelFrequency(),
        uni_len=unigram_model.getModelFrequencyLen(),
        bi_len=bigram_model.getModelFrequencyLen())

    # part 3 Generate mails
    unigram_model.generateEmail(howMany=generateCount)
    bigram_model.generateEmail(howMany=generateCount)
    trigram_model.generateEmail(howMany=generateCount)

    unigram_model.setSmoothed(smoothed=False).generateEmail(howMany=generateCount)
    bigram_model.setSmoothed(smoothed=False).generateEmail(howMany=generateCount)
    trigram_model.setSmoothed(smoothed=False).generateEmail(howMany=generateCount)

    outputFile.close()
    # part 4 evaluation
    # already calculated in part 2
    # please look up

    # not used for now, please check README.md
    # this is second approach of project
    # model_v2.createModel(trainSet)
    # model_v2.generateEmails(smoothed=True, howMany=10, filePath=outputFile)
    # model_v2.generateEmails(smoothed=False, howMany=10, filePath=outputFile)


def main(argv):
    utils.writeLog("MAIN-PROCESS", "N-Gram Language Model program is started.")

    inputCSV = argv[1]
    output_file = argv[2]

    # parse emails and get train-test sets
    EmailParser = mailparser.EmailParser(inputCSV)

    calculations(argv=argv, trainSet=EmailParser.getTrainSentences(), testSetEmails=EmailParser.getTestSetEmails())

    utils.writeLog("MAIN-PROCESS", "E-Mails are generated, probability and perplexity values are calculated.")
    utils.writeLog("MAIN-PROCESS", "To see output please look " + output_file)
    utils.writeLog("MAIN-PROCESS", "Exiting to N-Gram Language Model program.")


if __name__ == "__main__":
    main(sys.argv)
