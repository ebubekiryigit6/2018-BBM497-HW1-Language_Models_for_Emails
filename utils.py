import sys
import csv
import pickle
import time

import constants


def optimizeCsvSizeLimits():
    maxInt = sys.maxsize
    decrement = True

    while decrement:
        decrement = False
        try:
            csv.field_size_limit(maxInt)
        except OverflowError:
            maxInt = int(maxInt / 10)
            decrement = True
    writeLog("CSV", "CSV maximum size limits are optimized.")


def getPerplexity(prob, count):
    return 2 ** (-1 * (prob / count))


def printSentenceProbablityPerplexity(sentence, probability, perplexity, file):
    writeLineToFile(file, sentence)
    writeLineToFile(file, "Probability: " + str(probability))
    writeLineToFile(file, "Perplexity " + str(perplexity))
    writeLineToFile(file, "")


def printTestDataCalculationsBox(file):
    buffer = "| PERPLEXITY AND PROBABILITY VALUES OF EACH E-MAIL IN TEST SET |"
    writeLineToFile(file, "-" * len(buffer))
    writeLineToFile(file, buffer)
    writeLineToFile(file, "-" * len(buffer))
    writeLineToFile(file, "")


def printTestEmailProbabilityAndPerplexity(numOfMail, bi_prob, bi_perp, tri_prob, tri_perp, file):
    writeLineToFile(file, str(numOfMail) + ". E-Mail")
    writeLineToFile(file, "")
    writeLineToFile(file, "Smoothed Bigram Probability: " + str(bi_prob))
    writeLineToFile(file, "Smoothed Bigram Perplexity: " + str(bi_perp))
    writeLineToFile(file, "Smoothed Trigram Probability: " + str(tri_prob))
    writeLineToFile(file, "Smoothed Trigram Perplexity: " + str(tri_perp))
    writeLineToFile(file, "\n")


def printGeneratingBox(n, smoothed, file):
    if smoothed:
        sm = "SMOOTHED"
    else:
        sm = "UNSMOOTHED"

    if n == 1:
        n_gram = "UNIGRAM"
    elif n == 2:
        n_gram = "BIGRAM"
    elif n == 3:
        n_gram = "TRIGRAM"
    else:
        n_gram = str(n) + "-GRAM"

    buffer = "| GENERATING " + sm + " " + n_gram + " SENTENCES |"
    writeLineToFile(file, "-" * len(buffer))
    writeLineToFile(file, buffer)
    writeLineToFile(file, "-" * len(buffer))
    writeLineToFile(file, "")


def writeLineToFile(file, buffer):
    toWrite = buffer + "\n"
    file.write(toWrite)


def writeLog(tag, log):
    f = open(constants.LOG_FILE, "a")
    buffer = time.strftime("%H:%M:%S") + "   LOG/ " + tag.upper() + ":  " + log + "\n"
    f.write(buffer)
    f.close()


def writeAsSerializable(array, fileName):
    file = open(fileName, 'wb')
    pickle.dump(array, file)
    file.close()
    writeLog("PICKLE", "PICKLE file is created as " + fileName)


def getArrayFromPickle(fileName):
    file = open(fileName, 'rb')
    array = pickle.load(file)
    # file.close()
    writeLog("PICKLE", "Array is get from pickle file " + fileName)
    return array
