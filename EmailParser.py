import csv
import re

import constants
import utils


class EmailParser:
    __filePath = ""
    __emails = []
    __len_email = []

    def __init__(self, filePath):
        self.__emails = []
        self.__len_email = 0
        self.__filePath = filePath
        self.__readInputFile()

    """
    Public Methods
    """

    def getTrainingSetEmails(self):
        return self.__emails[:self.__getTrainSetLen()]

    def getTestSetEmails(self):
        return self.__emails[self.__getTrainSetLen():]

    def getEmailsCount(self):
        return self.__len_email

    def getTrainSentences(self):
        return self.__getSentencesFromEmails(self.getTrainingSetEmails())

    def getTestSentences(self):
        return self.__getSentencesFromEmails(self.getTestSetEmails())

    """
    ////
    End of Public Methods
    """

    def __getTrainSetLen(self):
        return int(round(self.__len_email / 100) * constants.TRAIN_SET_PERCENT)

    def __addSentence(self, sentence, array):
        sentence = sentence.lower().strip()
        # replace faster than regex replace.
        sentence = sentence \
            .replace(".", " . ") \
            .replace("!", " ! ") \
            .replace("?", " ? ") \
            .replace(":", " : ") \
            .replace(",", " , ") \
            .replace(";", " ; ") \
            .replace("\"", " \" ") \
            .replace("(", " ( ") \
            .replace(")", " ) ") \
            .replace("=", " = ")
        if sentence:
            array.append(sentence.strip())

    # populates sentences with given list
    def __populateSentences(self, sentenceArray, array):
        for sentence in sentenceArray:
            sentence = sentence.strip()
            self.__addSentence(sentence, array)

    def __readInputFile(self):
        utils.optimizeCsvSizeLimits()
        input_file = open(self.__filePath)
        csv_reader = csv.reader(input_file)

        for row in csv_reader:
            body_part = re.compile(constants.EMAIL_BODY_SPLIT_REGEX).split(row[1])  # row[0] is info of email
            # some emails has no body part
            if len(body_part) > 1:
                self.__splitForwardedMessages(body_part[1])
        input_file.close()
        self.__len_email = len(self.__emails)
        utils.writeLog("E-Mail Parser", "E-Mails are parsed successfully.")

    # finds forwarded messages and gets message bodies into mails
    def __splitForwardedMessages(self, message):
        message = message.replace("\n", " ").replace("\r", " ")
        splitted = re.split(constants.FORWARDED_EMAIL_SUBJECT_REGEX, message)

        if len(splitted) > 1:
            self.__findAndPushForwardedMessages(splitted)
        else:
            self.__populateEmailBodies(self.__replaceForwardedEmailWithNoSubject(splitted[0]))
            # self.__emailBodies.append(self.__replaceForwardedEmailWithNoSubject(splitted[0]))

    def __findAndPushForwardedMessages(self, splittedForwards):
        for i in splittedForwards:
            if i is not None:
                if i.strip():
                    self.__populateEmailBodies(self.__replaceForwardedEmailWithNoSubject(i))
                    # self.__emailBodies.append(self.__replaceForwardedEmailWithNoSubject(i))

    # holds all emails
    def __populateEmailBodies(self, email):
        self.__emails.append(email)

    # gets sentences from emails
    # to create test and trains sets
    def __getSentencesFromEmails(self, emails):
        temp = []
        for email in emails:
            sentenceArray = re.compile(constants.SENTENCE_BOUNDRY_REGEX).split(email)
            if len(sentenceArray) > 0:
                self.__populateSentences(sentenceArray, temp)
            else:
                self.__addSentence(email, temp)
        return temp

    # some mails are forwarded but it has no subject part.
    def __replaceForwardedEmailWithNoSubject(self, forwarded):
        return re.sub(constants.FORWARDED_EMAIL_NO_SUBJECT_REGEX, " ", forwarded).strip()  # not important field
