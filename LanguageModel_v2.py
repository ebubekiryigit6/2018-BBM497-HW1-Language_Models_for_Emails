import random
import re
import math

import constants
import utils

"""

This is my alternative static Language Model :)
I never use this but this is faster than other LanguageModel library
But other LanguageModel is like a library N-Gram, this model_v2 has static unigram,bigram and trigrams


"""

unigramDict = {}
bigramDict = {}
trigramDict = {}

UNIGRAM = 1
BIGRAM = 2
TRIGRAM = 3


# adds string boundaries to sentences according to n-grams
def setNgramStringBoundry(sentence, N_GRAM):
    if N_GRAM == UNIGRAM:
        sentence = sentence.strip()
        sentence += " </s>"
        return sentence
    else:
        n_gram = N_GRAM - 1
        sentence = ("<s> " * n_gram) + sentence + " </s>"
        return sentence


# populates frequency dictionary
def populateWordList(sentences):
    for sentence in sentences:
        for j in range(TRIGRAM):
            temp = setNgramStringBoundry(sentence, j + 1)
            words = re.compile(constants.ANY_WHITE_SPACE_REGEX).split(temp)
            repeat = len(words) - j
            for i in range(repeat):
                n_set = words[i:i + j + 1]
                result = " ".join(n_set)
                if j == 0:
                    if result in unigramDict:
                        unigramDict[result] += 1
                    else:
                        unigramDict[result] = 1
                elif j == 1:
                    if result in bigramDict:
                        bigramDict[result] += 1
                    else:
                        bigramDict[result] = 1
                else:
                    if result in trigramDict:
                        trigramDict[result] += 1
                    else:
                        trigramDict[result] = 1


# creates 3 models same time
def createModel(sentences):
    populateWordList(sentences)


# gets a dictionary that contains keys that starts with key value
# i couldn't use Default dict because i already finished my work :) sorry :)
def getStartsWith(key, n_gram_dict):
    temp = {}
    for i, j in n_gram_dict.items():
        if i.startswith(key):
            temp[i] = j
    return temp


def getProbablityUnigram(key, smoothed):
    if smoothed:
        return (unigramDict[key] + 1) / (sum(unigramDict.values()) + len(unigramDict))
    else:
        return unigramDict[key] / sum(unigramDict.values())


def getProbablityBigram(key, smoothed, beforeWordDict):
    if smoothed:
        return (bigramDict[key] + 1) / (sum(beforeWordDict.values()) + len(beforeWordDict))
    else:
        return (bigramDict[key]) / sum(beforeWordDict.values())


def getProbablityTrigram(key, smoothed, beforeWordDict):
    if smoothed:
        return (trigramDict[key] + 1) / (sum(beforeWordDict.values()) + len(beforeWordDict))
    else:
        return (trigramDict[key]) / sum(beforeWordDict.values())


def generateEmails(smoothed, howMany, filePath):
    utils.printGeneratingBox(1, smoothed, filePath)
    for i in range(howMany):
        generateEmailsUnigram(smoothed, filePath)
    utils.writeLog("N-GRAM", "Generating E-Mails for UNIGRAM is completed.")
    utils.printGeneratingBox(2, smoothed, filePath)
    for i in range(howMany):
        generateEmailsBigram(smoothed, filePath)
    utils.writeLog("N-GRAM", "Generating E-Mails for BIGRAM is completed.")
    utils.printGeneratingBox(3, smoothed, filePath)
    for i in range(howMany):
        generateEmailsTrigram(smoothed, filePath)
    utils.writeLog("N-GRAM", "Generating E-Mails for TRIGRAM is completed.")


def generateEmailsUnigram(smoothed, filePath):
    result = ""
    totalProbForPerplexity = 0
    wordCount = 0
    for i in range(constants.MAX_EMAIL_WORD_BOUNDRY):
        rand = random.uniform(0, 1)
        totalProb = 0
        key = ""
        for j, k in unigramDict.items():
            prob = getProbablityUnigram(j, smoothed)
            totalProb += prob
            if totalProb > rand and "<s>" not in j and "</s>" not in j:
                totalProbForPerplexity += math.log2(prob)
                wordCount += 1
                result = result + j + " "
                key = j
                break
        if "!" in key or "?" in key or "." in key:
            break
    perplexity = 2 ** (-1 * (totalProbForPerplexity / wordCount))
    # print sentence, probability and perplexity of each email that created by me.
    utils.printSentenceProbablityPerplexity(result.strip(), totalProbForPerplexity, perplexity, filePath)


def generateEmailsBigram(smoothed, filePath):
    result = ""
    firstWord = "<s> "
    totalProbForPerplexity = 0
    wordCount = 0
    for i in range(constants.MAX_EMAIL_WORD_BOUNDRY):
        rand = random.uniform(0, 1)
        totalProb = 0
        key = ""
        startsDict = getStartsWith(firstWord, bigramDict)
        for j in startsDict.keys():
            prob = getProbablityBigram(j, smoothed, startsDict)
            totalProb += prob
            if totalProb > rand:
                totalProbForPerplexity += math.log2(prob)
                wordCount += 1
                result = result + "".join(j.split(" ")[len(j.split()) - 1:]) + " "
                firstWord = " ".join(j.split(" ")[1:]) + " "
                key = j
                break
        if "!" in key or "?" in key or "." in key:
            break
    perplexity = 2 ** (-1 * (totalProbForPerplexity / wordCount))
    # print sentence, probability and perplexity of each email that created by me.
    utils.printSentenceProbablityPerplexity(result.strip(), totalProbForPerplexity, perplexity, filePath)


def generateEmailsTrigram(smoothed, filePath):
    result = ""
    firstWord = "<s> <s> "
    totalProbForPerplexity = 0
    wordCount = 0
    for i in range(constants.MAX_EMAIL_WORD_BOUNDRY):
        rand = random.uniform(0, 1)
        totalProb = 0
        key = ""
        startsDict = getStartsWith(firstWord, trigramDict)
        for j in startsDict.keys():
            prob = getProbablityTrigram(j, smoothed, startsDict)
            totalProb += prob
            if totalProb > rand:
                totalProbForPerplexity += math.log2(prob)
                wordCount += 1
                result = result + "".join(j.split(" ")[len(j.split()) - 1:]) + " "
                firstWord = " ".join(j.split(" ")[1:]) + " "
                key = j
                break
        if "!" in key or "?" in key or "." in key:
            break
    perplexity = 2 ** (-1 * (totalProbForPerplexity / wordCount))
    # print sentence, probability and perplexity of each email that created by me.
    utils.printSentenceProbablityPerplexity(result.strip(), totalProbForPerplexity, perplexity, filePath)
