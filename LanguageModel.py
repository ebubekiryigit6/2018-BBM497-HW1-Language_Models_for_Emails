import random
import re
import math

import constants
import utils

UNIGRAM = 1
BIGRAM = 2  # Default Constants
TRIGRAM = 3

"""
Creates N-Gram language model
N-Gram default is UNIGRAM but you can use any number of positive integers
Example: Create 10-Gram Language Model

"""


class LanguageModel:
    __N_GRAM = UNIGRAM
    __smoothed = False

    __sentences = []
    __NGram_model = {}

    __file = ""

    __sum_model_count = 0
    __len_model_count = 0

    def __init__(self, sentences, N_gram, smoothed, file):
        self.__N_GRAM = N_gram
        self.__smoothed = smoothed
        self.__sentences = sentences
        self.__NGram_model = {}
        self.__file = file
        self.__sum_model_count = 0
        self.__len_model_count = 0

    # creates model with given constructor parameters
    def createModel(self):
        self.__populateWordList()
        return self

    def setSmoothed(self, smoothed):
        self.__smoothed = smoothed
        return self

    def getModelFrequency(self):
        return self.__NGram_model

    def getModelFrequencyLen(self):
        return self.__len_model_count

    def getModelFrequencySum(self):
        return self.__sum_model_count

    def calculatePerplexityAndProbabilityFor(self, emails, unigramDict, bigramDict, uni_len, bi_len):
        utils.printTestDataCalculationsBox(self.__file)
        utils.writeLog("TEST DATA", "Calculating perplexity and probability values for test set.")
        numOfMail = 1
        for email in emails:
            sentenceArray = re.compile(constants.SENTENCE_BOUNDRY_REGEX).split(email)
            totalEmailProbBigram = 0
            totalWordCountBigram = 0
            totalEmailProbTrigram = 0
            totalWordCountTrigram = 0
            for sentence in sentenceArray:
                sentence = sentence.lower().strip()
                # replace faster than regex replace.
                sentence = sentence \
                    .replace(".", " . ").replace("!", " ! ").replace("?", " ? ") \
                    .replace(":", " : ").replace(",", " , ").replace(";", " ; ") \
                    .replace("\"", " \" ").replace("(", " ( ").replace(")", " ) ") \
                    .replace("=", " = ")
                sentence = sentence.strip()
                bigramSentenceProb, wordCountBigram = self.__calculateProbForSentence(BIGRAM, sentence, unigramDict,
                                                                                      bigramDict, uni_len, bi_len)
                totalEmailProbBigram += bigramSentenceProb
                totalWordCountBigram += wordCountBigram
                trigramSentenceProb, wordCountTrigram = self.__calculateProbForSentence(TRIGRAM, sentence, unigramDict,
                                                                                        bigramDict, uni_len, bi_len)
                totalEmailProbTrigram += trigramSentenceProb
                totalWordCountTrigram += wordCountTrigram

            bigramPerp = utils.getPerplexity(totalEmailProbBigram, totalWordCountBigram)
            trigramPerp = utils.getPerplexity(totalEmailProbTrigram, totalWordCountTrigram)
            utils.printTestEmailProbabilityAndPerplexity(numOfMail, totalEmailProbBigram, bigramPerp,
                                                         totalEmailProbTrigram, trigramPerp, self.__file)
            numOfMail += 1
        utils.writeLog("TEST DATA", "Calculations of Test Set are completed successfully.")

    def __calculateProbForSentence(self, N_GRAM, sentence, unigramDict, bigramDict, uni_len, bi_len):
        if N_GRAM == TRIGRAM:
            firstDict = self.getModelFrequency()
            secondDict = bigramDict
            secondLen = bi_len
        elif N_GRAM == BIGRAM:
            firstDict = bigramDict
            secondDict = unigramDict
            secondLen = uni_len

        smoothedProb = 0
        sentence = self.__setNgramStringBoundry(sentence, N_GRAM)
        words = re.compile(constants.ANY_WHITE_SPACE_REGEX).split(sentence)
        repeat = len(words) - N_GRAM + 1
        for i in range(repeat):
            n_set = words[i:i + N_GRAM]
            beginPhrase = " ".join(n_set[0:N_GRAM - 1])
            result = " ".join(n_set)
            count = 0 if result not in firstDict else firstDict[result]
            beginCount = 0 if beginPhrase not in secondDict else secondDict[beginPhrase]
            smoothedProb += math.log2((count + 1) / (beginCount + secondLen))
        return smoothedProb, len(words)

    # generates emails with given # of mail count
    def generateEmail(self, howMany):
        utils.printGeneratingBox(self.__N_GRAM, self.__smoothed, self.__file)
        if self.__N_GRAM == UNIGRAM:
            return self.__generateUnigram(howMany)
        else:
            return self.__generateAboveUnigram(howMany)

    # unigram and N>1 grams are a little bit different
    # so we seperated these two function model
    def __generateUnigram(self, howMany):
        for many in range(howMany):
            result = ""
            totalProbForPerplexity = 0
            wordCount = 0
            # max 30 words
            for i in range(constants.MAX_EMAIL_WORD_BOUNDRY):
                rand = random.uniform(0, 1)
                totalProb = 0
                key = ""
                # traverse unigram dict to calculate probability of random keys
                for j, k in self.__NGram_model.items():
                    prob = self.__getKeyProbablityUnigram(j)
                    totalProb += prob
                    if totalProb > rand and "<s>" not in j and "</s>" not in j:
                        totalProbForPerplexity += math.log2(prob)
                        wordCount += 1
                        result = result + j + " "
                        key = j
                        break
                # stop conditions
                if "!" in key or "?" in key or "." in key:
                    break
            perplexity = utils.getPerplexity(totalProbForPerplexity, wordCount)
            # print sentence, probability and perplexity of each email that created by me.
            utils.printSentenceProbablityPerplexity(result, totalProbForPerplexity, perplexity, self.__file)
        utils.writeLog("N-GRAM",
                       "Generating E-Mails for " + str(self.__N_GRAM) + "-Gram is completed. (Smoothed: " + str(
                           self.__smoothed) + ")")

    # generates emails for N>1 grams
    def __generateAboveUnigram(self, howMany):
        for many in range(howMany):
            result = ""
            # initial sentence boundary
            firstWord = "<s> " * (self.__N_GRAM - 1)
            totalProbForPerplexity = 0
            wordCount = 0
            for i in range(constants.MAX_EMAIL_WORD_BOUNDRY):
                rand = random.uniform(0, 1)
                totalProb = 0
                key = ""
                startsDict = self.__getStartsWith(firstWord)
                # gets starts with keys
                for j in startsDict.keys():
                    prob = self.__getKeyProbablityAboveUnigram(j, startsDict)
                    totalProb += prob
                    if totalProb > rand:
                        totalProbForPerplexity += math.log2(prob)
                        wordCount += 1
                        result = result + "".join(j.split(" ")[len(j.split()) - 1:]) + " "
                        firstWord = " ".join(j.split(" ")[1:]) + " "
                        key = j
                        break
                # stop conditions
                if "!" in key or "?" in key or "." in key:
                    break
            # sentence is ended with </s>. There is no word that starts with </s>
            # just ignore it
            result = result.replace("</s>", "")
            perplexity = utils.getPerplexity(totalProbForPerplexity, wordCount)
            # print sentence, probability and perplexity of each email that created by me.
            utils.printSentenceProbablityPerplexity(result.strip(), totalProbForPerplexity, perplexity, self.__file)
        utils.writeLog("N-GRAM",
                       "Generating E-Mails for " + str(self.__N_GRAM) + "-Gram is completed. (Smoothed: " + str(
                           self.__smoothed) + ")")

    # calculates key probability for unigram model
    def __getKeyProbablityUnigram(self, key):
        if self.__smoothed:
            return (self.__NGram_model[key] + 1) / (self.__sum_model_count + self.__len_model_count)
        else:
            return self.__NGram_model[key] / self.__sum_model_count

    # calculates key probability for N>1 grams
    def __getKeyProbablityAboveUnigram(self, key, beforeWordDict):
        if self.__smoothed:
            return (self.__NGram_model[key] + 1) / (sum(beforeWordDict.values()) + len(beforeWordDict))
        else:
            return (self.__NGram_model[key]) / sum(beforeWordDict.values())

    # gets a dictionary that contains keys that starts with key value
    # i couldn't use Default dict because i already finished my work :) sorry :)
    def __getStartsWith(self, key):
        temp = {}
        for i, j in self.__NGram_model.items():
            if i.startswith(key):
                temp[i] = j
        return temp

    # adds string boundaries to sentences according to n-grams
    def __setNgramStringBoundry(self, sentence, N_gram):
        if N_gram == UNIGRAM:
            sentence = sentence.strip()
            sentence += " </s>"
            return sentence
        else:
            n_gram = N_gram - 1
            sentence = "<s> " * n_gram + sentence + " </s>"
            return sentence

    # populates frequency dictionary
    def __populateWordList(self):
        for sentence in self.__sentences:
            sentence = self.__setNgramStringBoundry(sentence, self.__N_GRAM)
            # get all words with whitespace regex
            words = re.compile(constants.ANY_WHITE_SPACE_REGEX).split(sentence)
            repeat = len(words) - self.__N_GRAM + 1
            for i in range(repeat):
                n_set = words[i:i + self.__N_GRAM]
                result = " ".join(n_set)
                if result in self.__NGram_model:
                    self.__NGram_model[result] += 1
                else:
                    self.__NGram_model[result] = 1
        self.__len_model_count = len(self.__NGram_model)
        self.__sum_model_count = sum(self.__NGram_model.values())
        utils.writeLog("Language-Model", "Frequency Dictionary for " + str(
            self.__N_GRAM) + "-Gram is built successfully.")
