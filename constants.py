EMAIL_BODY_SPLIT_REGEX = r'X-FileName:[^\n]+'  # gets email body

FORWARDED_EMAIL_SUBJECT_REGEX = r'---------------------- Forwarded by[\s\S]*?Subject:'  # (-|.)*Forwarded by regex takes too long time to parse
FORWARDED_EMAIL_NO_SUBJECT_REGEX = r'----------------------[\s\S]*?---------------------------'
SENTENCE_REGEX = r'[^\.\!\?]*[\.\!\?]+'  # matches all sentences but runs very slow with re.findall()
SENTENCE_BOUNDRY_REGEX = r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s'  # matches gaps between sentences

ANY_WORD_REGEX = '(\W)'
ANY_LETTER_REGEX = r'[A-Za-z]'
ANY_DIGIT_REGEX = r'[0-9]'
ANY_LETTER_OR_DIGIT_REGEX = r'([A-Za-z]|[0-9])'
ANY_WHITE_SPACE_REGEX = r'\s+'

MAX_EMAIL_WORD_BOUNDRY = 30
TRAIN_SET_PERCENT = 60

PICKLE_FILE_NAME = "pickle.bin"  # to fast run, i will not use original assignment code
LOG_FILE = "console.log"
